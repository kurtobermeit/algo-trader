This setup assumes you have a working environment with Python, Node and other required tools installed.

## Setup
Open .env in your text editor and set the API_KEY and API_SECRET variables to match your Bitmex account. You can set TESTNET=true to test it out (recommended during setup)

Run `npm i` to install dependencies

Run `npm run dev` to start the bot on your computer

## Production Release
To setup on a production server:

Run `npm run start` - this will build & generate a "dist" folder for serving statically. It will also start the node server. If you want to use Forever or another management tool, you can run `npm run build` to generate "dist", and then serve node `node src/server/index.js`
