// by @Cryptodiddlers Inc. Coded by Chief Diddling Officer @thediddler
study("Bukakecator", shorttitle="Bukake Level", precision = 5)

Premium = security("BITMEX:XBTUSDPI", period, close)
ETHPremium = security("BITMEX:ETHUSDPI", period, close)
BTCPremiumTog = input(false)
ETHPremiumTog = input(false)

// CMF Inputs
mflength = input(20, minval=1)
ad = close==high and close==low or high==low ? 0 : ((2*close-low-high)/(high-low))*volume
mf = sum(ad, mflength) / sum(volume, mflength)
threshold = input(0.4,minval=0.05)

// Murr Math Inputs
length = input(100, minval = 10, title = "Look back Length")
mult = input(0.125, title = "Mutiplier; Only Supports 0.125 = 1/8")
lines = input(true, title= "Fractals")
bc = input(true, title = "Bar Colors")

hi = highest(high, length)
lo = lowest(low, length)
range = hi - lo
multiplier = (range) * mult
midline = lo + multiplier * 4

oscillator = (close - midline)/(range/2)

mmlevelneg1 = oscillator > 0 and oscillator < mult*2
mmlevelneg2 = oscillator > 0 and oscillator < mult*4
mmlevelneg3 = oscillator > 0 and oscillator < mult*6
mmlevelneg4 = oscillator > 0 and oscillator < mult*8

mmlevelpos1 = oscillator < 0 and oscillator > -mult*2
mmlevelpos2 = oscillator < 0 and oscillator > -mult*4
mmlevelpos3 = oscillator < 0 and oscillator > -mult*6
mmlevelpos4 = oscillator < 0 and oscillator > -mult*8

colorpos1 = color(#ADFF2F,80)
colorpos2 = color(#32CD32,80)
colorpos3 = color(#3CB371,80)
colorpos4 = color(#008000,30)

colorneg1 = color(#CD5C5C,80)
colorneg2 = color(#FA8072,80)
colorneg3 = color(#FFA07A,80)
colorneg4 = color(#FF0000,30)
colorgray = color(orange,60)

colordef = mmlevelneg1 ? colorneg1 : mmlevelneg2 ? colorneg2 : mmlevelneg3 ? colorneg3 : mmlevelneg4 ? colorneg4 : mmlevelpos1 ? colorpos1 : mmlevelpos2 ? colorpos2 : mmlevelpos3 ? colorpos3 : mmlevelpos4 ? colorpos4 : colorgray

plot (0.5*oscillator, color = colordef, title = "", style = columns)

hline(threshold, color=teal, linestyle=dotted, linewidth=1)
hline(-1*threshold, color=teal, linestyle=dotted, linewidth=1)
hline(0, color=teal, title="", linestyle=solid)

colormfred = color(yellow,30)
colormflime = color(yellow,30)
colormfgray = color(teal,10)
color = mf > threshold ? colormfred : (mf < -threshold ? colormflime : colormfgray)
plot(mf, color=color, title="", linewidth=1, style=histogram, trackprice=true)

// PLOT JIZZ ROCKET
colorpremlime = color(blue,90)
colorpremred = color(orange,90)
b_color = (Premium > 0.002) ? colorpremred : (Premium < -0.002) ? colorpremlime : na
ETHb_color = (ETHPremium > 0.002) ? colorpremred : (ETHPremium < -0.002) ? colorpremlime : na
bgcolor(BTCPremiumTog ? (Premium ? b_color : na) : ETHPremiumTog ? (ETHPremium ? ETHb_color : na) : na)

//bgcolor((mf > threshold and colordef == colorneg4) ? colorpremred : (mf < -threshold and colordef == colorpos4) ? colorpremlime : na, editable=false)
//bgcolor(b_color)