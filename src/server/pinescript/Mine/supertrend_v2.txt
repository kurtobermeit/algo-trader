//@version=2
// Best on 5m for BTC and ETH. Default settings (60, 1, 1, 1) 

strategy("WizTrend2", overlay=true, default_qty_value=1)
res = input(title="Main SuperTrend Time Frame", type=resolution, defval="60")
Factor=input(1, minval=1,maxval = 100)
Pd=input(1, minval=1,maxval = 100)

Up=hl2-(Factor*atr(Pd))
Dn=hl2+(Factor*atr(Pd))
MUp=security(tickerid,res,hl2-(Factor*atr(Pd)))
MDn=security(tickerid,res,hl2+(Factor*atr(Pd)))

Mclose=security(tickerid,res,close)

TrendUp=close[1]>TrendUp[1]? max(Up,TrendUp[1]) : Up
TrendDown=close[1]<TrendDown[1]? min(Dn,TrendDown[1]) : Dn

MTrendUp=Mclose[1]>MTrendUp[1]? max(MUp,MTrendUp[1]) : MUp
MTrendDown=Mclose[1]<MTrendDown[1]? min(MDn,MTrendDown[1]) : MDn

Trend = close > TrendDown[1] ? 1: close< TrendUp[1]? -1: nz(Trend[1],1)
Tsl = Trend==1? TrendUp: TrendDown

MTrend = Mclose > MTrendDown[1] ? 1: Mclose< MTrendUp[1]? -1: nz(MTrend[1],1)

plotshape(cross(close,Tsl) and close>Tsl , "Up Arrow", shape.triangleup,location.belowbar,green,0,0)
plotshape(cross(Tsl,close) and close<Tsl , "Down Arrow", shape.triangledown , location.abovebar, red,0,0)

golong = Trend == 1 and Trend[1] == -1 and MTrend == 1 
goshort = Trend == -1 and Trend[1] == 1 and MTrend == -1 


strategy.entry("Long", strategy.long,when=golong)
strategy.entry("Short", strategy.short,when=goshort)

// Only use SL/TP when testing, it ruins the profitability
//strategy.exit("Exit Long", from_entry = "Long", profit = 200, loss = 100)
//strategy.exit("Exit Short", from_entry = "Short", profit = 200, loss = 100)