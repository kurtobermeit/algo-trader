//@version=2

// TF NOTES:%  profit, profit factor
// XBT 5m:     54%,    2.1
// XBT 15m:    47%,    1.7 (30mins and above is weak)
// ETH 3m:     53%,    2.2
// ETH 5m      55%,    2.7
// ETH 15m:    50%,    1.0 (30mins and above is weak)
// other alts: all timeframes weak

strategy("Wizzle Strategy V1",shorttitle="Heikin Ashi F1",overlay=true,default_qty_value=1000,initial_capital=1000,currency=currency.USD,calc_on_every_tick=true)
res = input(title="Candle Time Frame", type=resolution, defval="60")
hshift = input(0,title="Candle Time Frame Shift")
res1 = input(title="EMA Time Frame", type=resolution, defval="180")
mhshift = input(0,title="EMA Time Frame Shift")
fama = input(1,"EMA Period")
test = input(0,"EMA Shift")
sloma = input(5,"Slow EMA Period")
slomas = input(1,"Slow EMA Shift")
macdf = input(false,title="With MACD filter")
res2 = input(title="MACD Time Frame", type=resolution, defval="15")
macds = input(1,title="MACD Shift")

//Open/Close Price
ha_close = security(tickerid, res, close[hshift])
mha_close = security(tickerid, res1, close[mhshift])


newSessionD = iff(change(ha_close), 1, 0)

getVWAP(newSession) =>
    p = iff(newSession, hlc3 * volume, p[1] + hlc3 * volume)
    vol = iff(newSession, volume, vol[1] + volume)
    v = p / vol
    
    // Incremental weighted standard deviation (rolling)
    // http://people.ds.cam.ac.uk/fanf2/hermes/doc/antiforgery/stats.pdf (part 5)
    // x[i] = hlc3[i], w[i] = volume[i], u[i] - v[i]
    
    Sn = iff(newSession, 0, Sn[1] + volume * (hlc3 - v[1]) * (hlc3 - v))
    std = sqrt(Sn / vol)
    
    [v, std]

[vwapDay, stdevD] = getVWAP(newSessionD)

// Function to dectect a new bar
is_newbar(res) =>
    t = time(res)
    change(t) != 0 ? true : false

// Check how many bars are in our upper timeframe
since_new_bar = barssince(is_newbar(res))
res_total_bars = na
res_total_bars := since_new_bar == 0 ? since_new_bar[1] : res_total_bars[1]

since2_new_bar = barssince(is_newbar(res1))
res2_total_bars = na
res2_total_bars := since2_new_bar == 0 ? since2_new_bar[1] : res2_total_bars[1]


final_ha_close = na
final_ha_close := barstate.isrealtime ? (since_new_bar == res_total_bars ? ha_close : final_ha_close[1]) : ha_close

final_mha_close = na
final_mha_close := barstate.isrealtime ? (since2_new_bar == res2_total_bars ? mha_close : final_mha_close[1]) : mha_close

//macd
[macdLine, signalLine, histLine] = macd(close, 12, 26, 9)
macdl = security(tickerid,res2,macdLine[macds])
macdsl= security(tickerid,res2,signalLine[macds])

//Moving Average
fma = ema(final_mha_close[test],fama)
sma = sma(final_ha_close[slomas],sloma)
plot(fma,title="MA",color=lime,linewidth=2,style=line)
plot(sma,title="SMA",color=red,linewidth=2,style=line)
plot(vwapDay, title = "VWAP - Daily", color = #00FF00DD, style = line, linewidth = 1)


//Strategy
golong =  crossover(fma,sma) and (macdl > macdsl or macdf == false )
goshort =   crossunder(fma,sma) and (macdl < macdsl or macdf == false )

strategy.entry("Buy",strategy.long,when = golong)
strategy.entry("Sell",strategy.short,when = goshort)
