//So SRSI 80/20 instead of 70/30
//EMA 20 and 50
//Only enter trade (short or long) if UCSS is red
//Also, I agree with when you said "I think when closing order, I'd set SL when rsi is above 70 / below 30, or wait until it's curling down from 70 / curling up from 30" instead of "- on green arrow, open long. Close when rsi above 70 (or when short condition is true)
//- on red arrow, open short. Close when rsi under 30 (or when long condition is true)"

// Strategy #1: StochSling
// Tools used: Stochastic RSI, slingshot, UCS squeeze
// Timeframe: 30min/1hr/4hr
// Logic:
// - EMA settings: 10 and 30
// - Only enter trade if UCSS is red
// - On green arrow, open long. Set trailing SL when SRSI over 80 (or when short condition true)
// - On red arrow, open short. Set trailing SL when SRSI under 20 (or when long condition true).

// Strategy #2: StochStoch
// Tools Used: stochastic, stochastic RSI, and BB width
// Timeframe: 30min
// Logic:
// - If S and SRSI BOTH under 20 AND BBW below .025 open long. Set trailing SL when BBW over .5 (or short condition true).
// - If S and SRSI BOTH over 80 AND BBW below .025 open short. Set trailing SL when BBW over .5 (or long condition true)

// Strategy #3: StochBB
// Tools used: BB strategy directed, Stochastic RSI.
// Timeframe 30min/1h/2hr/4hr
// Logic:
// - If BBSD signals long and SRSI is under 20, long. Set trailing SL when SRSI over 80 (or short condition true)
// - If BBSD signals short and SRSI is over 80, short. Sell trailing SL when SRSI goes under 20 (or long condition true


// -------------------------

//Created by ChrisMoody on 10-05-2014
//Known as SlingShot Method that keeps Traders on Trending Side of Market.
strategy("Wiz SlingShotSystem", overlay=true)

st = input(true, title="Show Trend Arrows at Top and Bottom of Screen?")
def = input(false, title="Only Choose 1 - Either Conservative Entry Arrows or 'B'-'S' Letters")
pa = input(true, title="Show Conservative Entry Arrows?")
emaSlow = ema(close, input(defval = 50, title = "Slow EMA Length"))
emaFast = ema(close, input(defval = 20, title = "Fast EMA Length"))

// STOCH
smoothK = input(1, minval=1), smoothD=input(3, minval=1)
len_stoch = input(7)
stochSma = sma(stoch(close, high, low, len_stoch), smoothK)
stochRsi = sma(stochSma, smoothD)

//EMA Definitions
//Aggressive Entry or Alert To Potential Trade
pullbackUpT() => emaFast > emaSlow and close < emaFast
pullbackDnT() => emaFast < emaSlow and close > emaFast
//Conservative Entry Code For Highlight Bars
entryUpT() => emaFast > emaSlow and close[1] < emaFast and close > emaFast
entryDnT() => emaFast < emaSlow and close[1] > emaFast and close < emaFast
//Conservative Entry True/False Condition
entryUpTrend = emaFast > emaSlow and close[1] < emaFast and close > emaFast ? 1 : 0
entryDnTrend = emaFast < emaSlow and close[1] > emaFast and close < emaFast ? 1 : 0
//Define Up and Down Trend for Trend Arrows at Top and Bottom of Screen
upTrend = emaFast >= emaSlow
downTrend = emaFast < emaSlow
//Definition for Conseervative Entry Up and Down PlotArrows
codiff = entryUpTrend == 1 ? entryUpTrend : 0
codiff2 = entryDnTrend == 1 ? entryDnTrend : 0
//Color definition for Moving Averages
col = emaFast > emaSlow ? lime : emaFast < emaSlow ? red : yellow

//Moving Average Plots and Fill
//p1 = plot(emaSlow, title="Slow MA", style=linebr, linewidth=4, color=col)
//p2 = plot(emaFast, title="Slow MA", style=linebr, linewidth=2, color=col)
//fill(p1, p2, color=silver, transp=50)
//Aggressive Entry, Conservative Entry Highlight Bars

//Trend Triangles at Top and Bottom of Screen
//plotshape(st and upTrend ? upTrend : na, title="Conservative Buy Entry Triangle",style=shape.triangleup, location=location.bottom, color=lime, transp=0, offset=0)
//plotshape(st and downTrend ? downTrend : na, title="Conservative Short Entry Triangle",style=shape.triangledown, location=location.top, color=red, transp=0, offset=0)

//Plot Arrows OR Letters B and S for Buy Sell Signals
//plotarrow(pa and codiff ? codiff : na, title="Up Entry Arrow", colorup=lime, maxheight=30, minheight=30, transp=0)
//plotarrow(pa and codiff2*-1 ? codiff2*-1 : na, title="Down Entry Arrow", colordown=red, maxheight=30, minheight=30, transp=0)

//plot(stochRsi, color=green, title="Stoch RSI")
barcolor(stochRsi < 30 ? purple: na, title="Oversold Color")
barcolor(stochRsi > 70 ? yellow : na, title="Overbought Color")


// STRATEGY CONDITIONS
longCondition = pa and codiff //and stochRsi < 80
shortCondition = pa and codiff2*-1 //and stochRsi > 30

// Enter long & short positions when condition triggers
strategy.entry("Long", strategy.long, when = longCondition)
strategy.entry("Short", strategy.short, when = shortCondition)

strategy.close(id = "Long", when = stochRsi > 80)
strategy.close(id = "Short", when = stochRsi < 10)

//strategy.entry(id = "Short", long = false, when = enterShort())
//strategy.close(id = "Short", when = exitShort())
