const express = require('express');
const UserRouter = require('./routes/user');
const { init } = require('./bot');
const app = express();

// Add routers to middleware
app.use('/api', UserRouter);
app.use(express.static('dist'));
app.listen(8081, () => console.log('\r\n ----- Startup complete -----'));
try {
    init()
} catch(err) {
    console.log('INDEX.JS ERROR: ', err)
}