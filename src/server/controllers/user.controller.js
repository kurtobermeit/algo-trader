const os = require('os');
const exchangeInterface = require('../services/exchange.interface');

// Get current user
exports.getUser = (req, res) => {
    // const ExchangeInterface = new exchangeInterface('HELLO', 'WORLD');
    // ExchangeInterface.apiConnect();
    res.send({ username: os.userInfo().username })
};

module.exports = exports;