'use strict';
const fs = require('fs');
const _ = require('lodash');
const moment = require('moment');
const schedule = require('node-schedule');
const axios = require('axios');
const Strategies = require('./lib/strategies');
const accountConfig = require('./config/accounts.json');
const every5seconds = 5000; // 5000 miliseconds
const everyMinute = 60000;
const everyHour = 3600000;

module.exports = {
    init: async () => {
        // Fetch updated symbol list from active exchanges - so we know what symbols we can trade
        await updateSymbols();
        // Re-check symbols every hour
        setInterval(updateSymbols, everyHour);
        // [new schedule.Range(2, 8, 14, 20)]
        
        // TODO: Convert this to config file. How do we store the time schedule? Can't use the "new range" code. Maybe create
        // consts like "every3hours" so that's what the config references, and we have an object that maps that key to the "new range"

        // TODO: SYMBOLS - store in config/symbols.json. Key is the exchange name, value is array of strings

        var ethEmaJob = schedule.scheduleJob({minute: [14, 29, 35, 44, 59], second: [55]},
            () => Strategies.emasmaStrat('ETHUSD', '3h', {
                account: 'SeppeEMA', ema: 5, sma: 8, margin: 0.15, contractQty: null, stoploss: null, takeprofit: null, leverage: 10, orderType: 'market', backtest: false, papertrade: true, limit: 750
            }, ethEmaJob)
        );

        var scalpStrat = schedule.scheduleJob({minute: [new schedule.Range(0,59)], second: [new schedule.Range(0, 59)]},
            () => Strategies.scalpStrat('XBTUSD', '3m', {
                account: 'WizzleGuppy', chart: '1h', margin: '10%', contractQty: null, stoploss: null, takeprofit: null, orderType: 'market', leverage: 1, backtest: true, papertrade: true, limit: 750
            }, scalpStrat)
        );
        // var ethSuperJob = schedule.scheduleJob({minute: [new schedule.Range(0,59)], second: [56]},
        //     () => Strategies.guppyStrat('ETHUSD', '15m', {
        //         account: 'WizzleGuppy', chart: '1h', margin: '10%', contractQty: null, stoploss: null, takeprofit: null, orderType: 'market', leverage: 10, backtest: false, papertrade: true, limit: 750
        //     }, ethSuperJob)
        // );

        var jobs = {ETH_EMA: ethEmaJob, SCALP_STRAT: scalpStrat};

        console.log('\r\n----- Algo Bot has started ----- \r\n\ ');
        _.each(jobs, (job, name) => {
            console.log(name + ' check: ', moment().utc().to(job.nextInvocation()) + ' - ' + moment(job.nextInvocation()).format('dddd MMMM Do YYYY @ h:mma'));
        });
    }
}

const updateSymbols = async () => {
    var exchanges = JSON.parse(fs.readFileSync('src/server/config/exchanges.json', 'utf8'));
    _.each(exchanges, async (exchange, name) => {
        // Only update symbols if there is an existing account/strategy
        let accounts = _.filter(_.values(accountConfig), (a) => a.exchange == name);
        if (!accounts || !accounts.length) {
            console.log('No accounts for exchange ' + name + ', skip updateSymols');
            return;
        }

        // Instantiate the exchange service class using first account
        const exchangeClass = require('./services/' + accounts[0].exchange);
        const Service = new exchangeClass(accounts[0].key, accounts[0].secret, accounts[0].testnet);
        // Connect to API and get the list of contracts (symbols). Update config
        await Service.connectApi();
        const symbols = await Service.getActiveContracts();
        exchanges[name].symbols = symbols;

        // Save config to file so we have the latest data
        fs.writeFileSync('src/server/config/exchanges.json', JSON.stringify(exchanges));
    });
}
