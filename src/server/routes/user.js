var express = require('express');
var router = express.Router();
var UserController = require('../controllers/user.controller');

// GET username
router.get('/getUsername', UserController.getUser);

module.exports = router;