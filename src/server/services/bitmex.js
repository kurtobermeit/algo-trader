'use strict';
var SwaggerClient = require("swagger-client");
const crypto = require('crypto');
const { decrypt, encrypt } = require('../lib/crypto');
const BitMEXClient = require('bitmex-realtime-api');
const Interface = require('./exchange.interface');
const _ = require('lodash');
const moment = require('moment');
const axios = require('axios');
const devMode = false;
var client;
var websocketClient;

// Timeframes supported by Bitmex API
const defaultTimeframes = ['1m', '5m', '1h', '1d'];
const calculatedTimeframes = {
    '3m': {base: '1m', unit: 'minute', interval: 3, close: [0,3,6,9,12,15,18,21,24,27,30,33,36,39,42,45,48,51,54,57]},
    '15m': {base: '5m', unit: 'minute', interval: 3, close: [0,15,30,45]},
    '30m': {base: '5m', unit: 'minute', interval: 6, close: [0,30]},
    '2h': {base: '1h', unit: 'hour', interval: 2, close: [0,2,4,6,8,10,12,14,16,18,20,22]},
    '3h': {base: '1h', unit: 'hour', interval: 3, close: [0,3,6,9,12,15,18,21]},
    '4h': {base: '1h', unit: 'hour', interval: 4, close: [0,4,8,12,16,20]},
    '6h': {base: '1h', unit: 'hour', interval: 6, close: [0,6,12,18]},
    '12h': {base: '1h', unit: 'hour', interval: 12, close: [0,12]},
};

/**
 * Create new order for User
 * Tested!
 * 
 * @param {*,array} Data
 */
const createOrder = (data) => {
    // Missing leverage
    console.log('BITMEX createOrder', data);
    return this.client.Order.Order_new(data)
    .then((response) => {
        console.log('Create order response: ', response.data.toString());
        return JSON.parse(response.data.toString())
    })
    .catch((e) => {
        console.log('**** createOrder error:', e);
        return e;
    })
}

const parseOrder = (data) => {
    // Get the data needed and return correct format for bitmex
    return data;
}

var self = null;

module.exports = class Bitmex {
    test() {
        console.log('TEST');
    }

    constructor(key, secret, isTestnet) {
        // super();
        if (!key || !secret) {
            throw new Error('api.js: API key & secret required to use Bitmex API!');
        }

        this.key = decrypt(key);
        this.secret = decrypt(secret);
        this.isTestnet = isTestnet;
        self = this;
        this.baseUrl = isTestnet ? 'https://testnet.bitmex.com' : 'https://www.bitmex.com'
    }

    async connectApi() {
        await new SwaggerClient(self.baseUrl + '/api/explorer/swagger.json', 
        {
            requestInterceptor: self.getInterceptor
        })
          .then((cl) => {
              self.client = cl.apis;
        })
        .catch((e) => console.error("*** Unable to connect:", e))
    }

    getHeaders(url, method = 'GET') {
        const expires = new Date().getTime() + (300 * 1000)
        const signature = crypto.createHmac('sha256', self.secret).update(method + url + expires).digest('hex');
        return {
            'api-key': this.key,
            'api-expires': expires,
            'api-signature': signature
        };
    };

    getInterceptor(req) {
        if (!req.url.includes('swagger')) {
            // If body set, append to url
            const body = req.body && req.body.length ? ('?' + req.body) : '';
            // Remove base from URL
            const url = req.url.replace(self.baseUrl, '') + body;
            req.headers = self.getHeaders(url, req.method);
            req.url = req.url + body;
            // console.log('REQ HEADERSSSSS: ', req);
        }
        return req;
    }

    /**
     * Get active contracts.
     * Tested!
     */
    async getActiveContracts() {
        if (!this.client) {
            console.log('CLIENT FAILED getActiveContracts');
            // return [];
        }
        const resp = await this.client.Instrument.Instrument_getActive()
        .catch((e) => {console.error("*** Unable to getActiveContracts:" + e.status + ' - ' + e.response.statusText); return [];});
        // Body contains contracts that have been settled, filter these out and get the symbol field
        return _.map(_.filter(resp.body, { state: 'Open' }), 'symbol');
    }

    /**
     * Get orders for current User.
     * Tested!
     */
    getOrders() {
        return this.client.Order.Order_getOrders()
        .then((response) => JSON.parse(response.data.toString()))
        .catch((e) => {
            console.log('**** getOrders error:', e);
            return e;
        })
    }

    /**
     * Get positio,ns for current User.
     */
    getOpenOrders() {
        return this.client.Position.Position_get()
        .then((response) => JSON.parse(response.data.toString()))
        .catch((e) => {
            console.log('**** getOrders error:', e);
            return e;
        })
    }

    /**
     * Get margin & balance for current User.
     * Tested!
     */
    getMargin() {
        return this.client.User.User_getMargin()
        .then((response) => {
        var margin = JSON.parse(response.data.toString());
        var marginBalance = (margin.marginBalance / 1e8).toFixed(4);
        console.log('**** MARGIN: ', {margin: margin, marginBalance: marginBalance});
        return {margin: margin, marginBalance: marginBalance};
        })
        .catch((e) => {
            console.log('**** getMargin error:', e);
            return e;
        })
    }

    /**
     * Buy
     * 
     * 
     * @param {*string} symbol Symbol e.g. XBTUSD
     * @param {*float} price Price e.g. 6505
     * @,param {*integer} qty Contract Quantity e.g. 10,000
     */
    buy(symbol, price = 0, qty) {
        // Missing leverage
        var data = {symbol: symbol, price: price, orderQty: qty, side: 'Buy'};
        // If price 0, market. Else, limit
        if (!price) {
            data = {symbol: symbol, orderQty: qty, side: 'Buy', ordType: 'Market'}
        }
        console.log('**** ' + (data.ordType == 'Market' ? 'Market ' : 'Limit ') + 'Buy: ', data);
        return createOrder(data)
    }

    /**
     * Sell
     * 
     * 
     * @param {*string} symbol Symbol e.g. XBTUSD
     * @param {*float} price Price e.g. 6505
     * @p,aram {*integer} qty Contract Quantity e.g. 10,000
     */
    sell(symbol, price = 0, qty) {
        // Missing leverage
        // if price 0, market. Else, limit
        if (price) {
            console.log('**** limitSell', symbol, price, qty, 'Sell');
            return createOrder({symbol: symbol, price: price, orderQty: qty, side: 'Sell'})
        } else {
            console.log('**** Market Sell: ', {symbol: symbol, orderQty: qty, side: 'Sell', ordType: 'Market'});
            return createOrder({symbol: symbol, orderQty: qty, side: 'Sell', ordType: 'Market'})
        }
    }

    /**
     * Stop
     * 
     * 
     * @param {*string} symbol Symbol e.g. XBTUSD
     * @param {*float} price Price e.g. 6505
     * @param {,*integer} qty Contract Quantity e.g. 10,000
     */
    marketStop(symbol, price, qty) {
        // Missing leverage
        var data = {symbol: symbol, stopPx: price, orderQty: qty, ordType: 'Stop'};
        console.log('**** market Stop', data);
        return createOrder(data)
    }

    /**
     * Close an existing User order.
     * 
     * 
     * @pa,ram {*string} symbol Symbol e.g. XBTUSD
     */
    close(symbol, price) {
        // if price 0, market. Else, limit
        if (price) {
            console.log('**** limit close ' + symbol, price);
            // TODO: This works first time, but not once it's setup
            return createOrder({symbol: symbol, execInst: 'Close', ordType: 'Limit', price: price})
        } else {
            console.log('**** market close ' + symbol);
            return createOrder({symbol: symbol, execInst: 'Close', ordType: 'Market'})
        }
    }

    /**
     * Update leverage for coin
     * Tested!
     * 
     * @param {*array} Data
     */
    updateLeverage(symbol, leverage) {
        // Missing leverage
        console.log('BITMEX updateLeverage: ' + symbol + ' @ ' + leverage);
        return this.client.Position.Position_updateLeverage({symbol, leverage})
        .then((response) => {
            var order = JSON.parse(response.data.toString())
            return order;
        })
        .catch((e) => {
            console.log('**** updateLeverage error:', e);
            return e;
        })
    }

    /**
     * Amend an existing User order.
     * Tested!
     * 
     * @param {*string} orderId Bitmex order id e.g. 8789asdf-asfd8798asfd-asdf787
     * @param {*float} price Order price e.g. 6500
     * @param {,*integer} qty Order Qty e.g. 4000
     */
    amendOrder(orderId, price = null, qty = null, limitClose = null) {
        let data = {orderID: orderId};
        if (price) {
            data.price = price
        }
        if (qty) {
            data.orderQty = qty
        }
        if (limitClose) {
            data.stopPx = limitClose
        }
        return this.client.Order.Order_amend(data)
        .then((response) => JSON.parse(response.data.toString()))
        .catch((e) => {
            console.log('amendOrder error:', e);
            return e;
        })
    }

    /**
     * Cancel an existing User order.
     * Tested!
     * 
     * @param {*,string} orderId Order ID e.g. f89as8df9-asdf89asdf89
     */
    cancelOrder(orderId) {
        return this.client.Order.Order_cancel({orderID: orderId})
        .then((response) => JSON.parse(response.data.toString()))
        .catch((e) => {
            // If status 401 - unauthorized
            if (e.statusCode === 401) {
                console.log('No permission to cancel!!');
                return null;
            } else {
                console.log('**** cancelOrder error:', e);
                return e;
            }
        })
    }

    /**
     * Create bulk orders
     */
    createBulkOrders(orders) {
        return this.client.Order.Order_newBulk({
            "orders": JSON.stringify(orders)
        })
        .then((response) => {
            console.log(response.data.toString());
            return response.data;
        })
        .catch((e) => {
            console.log('createBulkOrders error:', e);
            return e;
        })
    }

    /**
     * Get indicator value for given pattern and data
     */
    getIndicators(pattern, data) {
        let p = require('technicalindicators')[pattern];
        return _.reverse(p.calculate(data));
    }

    async getChartData(symbol, timeframe, limit = 100) {
        // Check if timeframe is supported. If not, we need to calculate it
        if (defaultTimeframes.indexOf(timeframe) !== -1) {
            return self.fetchChart(symbol, timeframe, limit);
        } else if (calculatedTimeframes[timeframe]) {
            return self.calculateChartData(symbol, timeframe);
        } else {
            console.log('** CHART TIMEFRAME NOT SUPPORTED: ' + timeframe);
            return null;
        }
    }

    async fetchChart(symbol, timeframe, limit = 100, format = true) {
        const url = self.baseUrl + '/api/v1/trade/bucketed?binSize=' + timeframe + '&partial=false&symbol=' + symbol + '&count=' + limit + '&reverse=true';
        var chart = await axios.get(url);

        // console.log('\r\n***************#*#*#*#*#*#*#*#*##*#*#* \r\n\r\n fetchChart', chart.data);
        chart.data = _.reverse(chart.data);
        // Data is an array of objects, convert to object of arrays. Open, High, Low, Close are all objects with arrays
        if (chart.data && chart.data.length) {
            return format ? self.formatChartData(chart.data) : chart.data;
        }
        return null;
    }

    async calculateChartData(symbol, timeframe) {
        // Currently 18, so countdown and find the nearest (previous) number (that's not equal to current)
        var chart = [];
        let first = true;
        const settings = calculatedTimeframes[timeframe];
        // Get chart data for the base timeframe. Row count is determined by multiplying interval by 100
        let data = await self.fetchChart(symbol, settings.base, 750, false);
        // Remove last record from array as its the current candle
        data = _.take(data, data.length-1);

        for(let i = 0; i < data.length; i++) {
            const candle = data[i];
            let timeClose = self.getCloseUnit(candle.timestamp, settings.unit)
            if (settings.close.indexOf(timeClose) !== -1 && i+2 < data.length) {
                const chunk = _.slice(data, i, i+settings.interval);
                // Loop through chunk and get the highest high, lowest low, first open, last close. Push to chart
                chart.push({
                    open: _.first(chunk).open,
                    close: _.last(chunk).close,
                    high: _.maxBy(chunk, (row) => row.high).high,
                    low: _.minBy(chunk, (row) => row.low).low,
                    volume: _.sumBy(chunk, (row) => row.volume),
                    timestamp: _.last(chunk).timestamp
                });
                i = i + 2;
            }
        }

        // console.log('\r\n***************\r\n\r\n calculateChart', chart, data.length, chart.length);
        // Reverse so oldest to newest
        return self.formatChartData(chart);
    }

    getCloseUnit(timestamp, unit) {
        switch(unit) {
            case 'hour':
                return parseInt(moment.utc(timestamp).subtract(1, 'hours').format('H'));
            case 'minute':
                return parseInt(moment.utc(timestamp).subtract(5, 'minutes').format('m'));
        }
    }

    formatChartData(chart) {
        const close = _.map(chart, (item) => item.close);
        const open = _.map(chart, (item) => item.open);
        const high = _.map(chart, (item) => item.high);
        const low = _.map(chart, (item) => item.low);
        const volume = _.map(chart, (item) => item.volume);
        const timestamp = _.map(chart, (item) => item.timestamp);
        return {open, high, low, close, volume, timestamp};
    }

    // Create client to interface with Bitmex websocket
    setupWebsocket(account, symbols, instrumentCallback) {
        const accountName = account.name;
        console.log('setupWebsocket', account, symbols);
        require('events').EventEmitter.prototype._maxListeners = 100;
        // Decrypt API key & secret and return the client
        websocketClient = new BitMEXClient({
            testnet: account.testnet,
            apiKeyID: decrypt(account.key),
            apiKeySecret: decrypt(account.secret),
            maxTableLen: 1000
        });

        // Setup the websocket to execute callback on price update
        _.each(symbols, (symbol) => {
            websocketClient.addStream(symbol, 'instrument', (data, sym, tableName) => instrumentCallback(accountName, symbol, data[0].lastPrice));
        })
    }
}