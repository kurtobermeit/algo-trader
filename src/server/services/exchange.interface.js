/**
 * This is the interface for Exchanges. It will act as a unified wrapper around all exchanges.
 * So anytime we want to add a new exchange, it must be compatible with these functions
 * 
 */

module.exports = class Interface {
    constructor(key, secret) {
      this.key = key;
      this.secret = secret;
    }

    async connectApi() {
        console.log('Connecting with key and secret: ', this.key, this.secret);
        return true;
    }

    async getHeaders() {

    }

    /* MISC FUNCTIONS */
    // checkIndicators
    // getIndicators
    // getChartData
    // getSymbols
    // calculateCandle

    /* TRADING RELATED */
    // getOrder
    // getOpenOrders
    // createOrder
    // createOrders
    // updateOrder
    // closeOrder
    // cancelOrder
    // cancelAll
  }

