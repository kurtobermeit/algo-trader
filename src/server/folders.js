const path = require('path');
const _ = require('lodash');

// put paths to folders here: must start with src/
const folderPaths = {
    app: 'src/server',

    // shared & globals
    config: 'src/server/config',
    lib: 'src/server/lib',
    models: 'src/server/models',
    controllers: 'src/server/controllers',
    routes: 'src/server/routes',
    services: 'src/server/services',
};

module.exports = () => {
    const returnable = {};
    _.forEach(folderPaths, (val, key) => {
        returnable[key] = path.resolve(val);
    });
    return returnable;
};
