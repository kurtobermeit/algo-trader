/**
 * This is the interface for Exchanges. It will act as a unified wrapper around all exchanges.
 * So anytime we want to add a new exchange, it must be compatible with these functions
 * 
 */

module.exports = class Interface {
    constructor(key, secret) {
      this.key = key;
      this.secret = secret;
    }

    calculatePosition = async (price, margin, leverage, contracts) => {
        // calculate margin using # of contracts IF provided
        // price * margin * leverage
        // fees 0.0375% x leverage x contracts
    }

    excecuteTrade = async () => {
        console.log('EXECUTE');
    }

    openPosition = async () => {
        console.log('OPEN');
    }

    closePosition = async () => {
        console.log('CLOSE');
    }

    updatePrices = async () => {
        console.log('PRICES');
    }

    checkProfitLoss = async () => {
        console.log('PNL');
    }

    getChartData = async () => {
        console.log('CHART');
    }

    getIndicators = async () => {
        console.log('INDICATORS');
    }

    format = async () => {
        console.log('FORMAT');
    }

    setupTrades = async () => {
        console.log('SETUP');
    }
  }

