const schedule = require('node-schedule');
const rule = new schedule.RecurrenceRule();

module.exports = {
    setupTasks: (callback) => {
        rule.hour = [new schedule.Range(2, 8, 14, 20)];
        rule.second = 30;
        
        schedule.scheduleJob(rule, () => {
          console.log('Every 6hr from 2am');
        });
    }
}