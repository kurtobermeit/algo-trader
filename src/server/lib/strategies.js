const moment = require('moment');
const axios = require('axios');
const _ = require('lodash');
const fs = require('fs');
const accountConfig = require('../config/accounts.json');
const exchangeConfig = require('../config/exchanges.json');
const EMA = require('technicalindicators').EMA;
const SMA = require('technicalindicators').SMA;
const RSI = require('technicalindicators').RSI;
const CCI = require('technicalindicators').CCI;
const ATR = require('technicalindicators').ATR;

const availableIndicators = {EMA, SMA, RSI, CCI};
const resolution = {"1m": 1, "3m": 1, "5m": 5, "10m": 5, "15m": 5, "30m": 5, "1h": 60, "2h": 60, "3h": 60, "4h": 60, "1D": "D", "2D": "D", "1W": "D"};
// Store symbols & whether long or short, so we know how to handle strat
var openTrades = {};
var currentIndicators = {};
var profitHistory = [];
var bought = -1;
var jobConfig = {};
var chartData = {};
const chartsCache = {};
var serviceClasses = {};
var coinPrices = {};
// Track what the previous signal (short or long) was for symbol's previous candle
var previousSignal = {};
var priceLastUpdated = new Date().getTime();
var websocketSetup = false;

const getBitmexChart = async(symbol, timeframe, fromTime, toTime) => {
    // Return if in cache
    const uri = timeframe + '/a879a9s8df7a98?from=' + fromTime + '&to=' + toTime;
    if (chartsCache[uri]) {
        console.log('returning cache ' + uri);
        return chartsCache[uri];
    }
    try {
        console.log('URL: ', 'https://demo.bitv.app/alert/api/mexchart/1/' + uri);
        const json = (await axios.get('https://demo.bitv.app/alert/api/mexchart/1/' + uri, { headers: { Authorization: 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJkYXRhIjp7InVzZXIiOnsiaWQiOjcsImRpc2NvcmRJZCI6IjIwMTU3NTY2Mzk3MzU2NDQxNyIsInRva2VuIjoiN3ZDNkhWc0NveUI0S0ptMERkNHpieFhqZXVMTFQyIn19fQ.BPDZ54LYYBSJzh25ct5XIRaarVyYjOkP9PT80V8Bkg0'}})).data
        if (!json) {
            return chartData;
        }
        const chartData = parseChartData(json);
    } catch (err) {
        // Check if we have response.data
        const error = (err.response && err.response.data) || err;
        const code = err.response ? err.response.status : 409;
        console.log('ERROR fetching bitmex chart ' + symbol + '. Code: ' + code, error)
        return code;
    }

    chartsCache[uri] = chartData;
    return chartData;
  }

const parseChartData = (json) => {
    chartData = { open: [], close: [], high: [], low: [], volume: [], timestamp: []};
    for(let i = 0; i < json.length; i++) {
        chartData.timestamp.push(json[i][0] / 1000);
        chartData.open.push(json[i][1]);
        chartData.high.push(json[i][2]);
        chartData.low.push(json[i][3]);
        chartData.close.push(json[i][4]);
        chartData.volume.push(json[i][5]);
    };
    return chartData;
}

const updatePrices = (accountName, symbol, price) => {
    // Wait 3 seconds between updating to prevent spam
    if (new Date().getTime()-priceLastUpdated > 100) {
        // console.log('UPDATE PRICE FOR ' + symbol + ' ' + price, coinPrices);
        coinPrices[symbol] = price;
        priceLastUpdated = new Date().getTime();
    }

    // Check profit & loss for symbol
    checkProfitLoss(accountName, symbol, price);
}

// If position is open for symbol, check the profit & loss. Close if condition is met
const checkProfitLoss = (accountName, symbol, price) => {
    // Skip if no position open
    if (!openTrades[accountName] || !openTrades[accountName][symbol] || openTrades[accountName][symbol].side == null) {
        return;
    }

    const currentPrice = coinPrices[symbol];
    const profit = getPositionProfit(accountName, symbol);
    const takeProfit = jobConfig[accountName][symbol].takeprofit;
    const stopLoss = jobConfig[accountName][symbol].stoploss;
    
    // If job has take profit set and profit is above it, close
    if (takeProfit !== null && profit >= takeProfit) {
        console.log(accountName + ' ' + symbol + ' Profit is over max, taking profit here. Profit: ' + profit + '%, max: ' + takeProfit + '%');
        closePosition(accountName, symbol, currentPrice, 'stop');
    }

    // If job has stop loss set and profit is below it, close
    if (stopLoss !== null && profit <= stopLoss) {
        console.log(accountName + ' ' + symbol + ' loss is below max, taking loss here. Loss: ' + profit + '%, max: ' + stopLoss + '%');
        closePosition(accountName, symbol, currentPrice, 'profit');
    }
}

const closePosition = async (accountName, symbol, price, type) => {
    const profit = getPositionProfit(accountName, symbol);
    profitHistory.push(profit);
    const stats = calculateStats(accountName, symbol, profitHistory);
    console.log(accountName + ' ' + symbol + ' ' + openTrades[accountName][symbol].side + ' position from ' + openTrades[accountName][symbol].price + ' has now been closed at ' + profit + '% profit.')
    openTrades[accountName][symbol].side = null;
    openTrades[accountName][symbol].price = null;
    await axios.post('http://localhost:3003/api/post-trade/a879a9s8df7a98', {type, accountName, symbol, stats, newSide: null, profit, entry: openTrades[accountName][symbol].price})
}

const getChartData = async (exchange, symbol, timeframe, accountName) => {
    const rowLimit = jobConfig[accountName][symbol].limit || 100;
    // Check when it was last fetched in chartData. If old (or not exist), re-fetch
    if (chartData[exchange] && chartData[exchange][symbol] && chartData[exchange][symbol][timeframe]) {
        const chart = chartData[exchange][symbol][timeframe];
        if (moment().diff(chart.timestamp, "minutes") < 1) {
            return chart.data;
        }
    }

    const chart = await serviceClasses[accountName].getChartData(symbol, timeframe, rowLimit);
    if (!chartData[exchange]) {
        chartData[exchange] = {}
    }
    if (!chartData[exchange][symbol]) {
        chartData[exchange][symbol] = {};
    }
    if (!chartData[exchange][symbol][timeframe]) {
        chartData[exchange][symbol][timeframe] = {};
    }
    chartData[exchange][symbol][timeframe] = {
        timestamp: moment().format(),
        data: chart
    }

    return chart;
}

const getIndicators = async (symbol, timeframe, config) => {
    const account = accountConfig[config.account];
    if (!account) {
        console.log('GetIndicators no account');
        return;
    }
    const { exchange } = account;

    // TODO: Update to handle config like this {ema1: {name: 'EMA', val: 5},ema2: {name: 'EMA', val; 8}}
    const exchangeClass = require('../services/' + exchange);
    // If not exist, instantiate class
    if (!serviceClasses[config.account]) {
        console.log('Setting up service class for ' + exchange);
        
        serviceClasses[config.account] = new exchangeClass(account.key, account.secret, account.testnet);
        if (!websocketSetup) {
            console.log('** INIT websocket');
            websocketSetup = true;
            await serviceClasses[config.account].setupWebsocket(account, exchangeConfig[exchange].symbols, updatePrices);
        }
    }

    const resp = await getChartData(exchange, symbol, timeframe, config.account);
    // const resp = await serviceClasses[config.account].getChartData(symbol, timeframe, (config.limit || 100));
    if (!resp || !resp.open.length) {
        console.log('ERROR: Unable to get TA for symbol "' + symbol + '", timeframe "' + timeframe + '". ');
        return null;
    }
    const lastIndex = resp.close.length-1;

    // Get current price using the close from most recent candle (data in reverse, so it's last item)
    const currentPrice = format(resp.close[lastIndex]);
    const ema = format(EMA.calculate({values: resp.close, period: config.ema}).pop());
    const sma = format(SMA.calculate({values: resp.close, period: config.sma}).pop());
    const atr = ATR.calculate({...resp, period: config.pd}).pop();
    const rsi = format(RSI.calculate({values: resp.close, period: 14}).pop());
    const cci = format(CCI.calculate({...resp, period: 20}).pop());
    return {ema, sma, currentPrice, rsi, cci, atr, chart: _.clone(resp)};
}

const format = (value) => {
    if (value > 1) {
        return parseFloat(value.toFixed(2));
    } else if (value) {
        return parseFloat(value.toFixed(10));
    }
}

 const setupTrades = (symbol, config) => {
    const account = accountConfig[config.account];
    if (!account) {
        console.log('setupTrades no account');
        return;
    }
    const { exchange } = account;

    // If no open trade for exchange, add entry
    if (!openTrades[config.account]) {
        openTrades[config.account] = {[symbol]: {}};
    }

    if (!jobConfig[config.account]) {
        jobConfig[config.account] = {[symbol]: config};
    }

    if (!jobConfig[config.account][symbol]) {
        jobConfig[config.account][symbol] = config;
    }

    // If no open trade for symbol, add entry
    if (!openTrades[config.account][symbol]) {
        openTrades[config.account][symbol] = {side: null};
    }
 }

  const getPositionProfit = (accountName, symbol, currentPrice = null) => {
    // Make sure a position is open for symbol
    if (!openTrades[accountName][symbol] || openTrades[accountName][symbol].side == null) {
        return null;
    }

    // Get the opposite side to current position (if long, we're now short)
    const newSide = openTrades[accountName][symbol].side == 'long' ? 'short' : 'long';
    currentPrice = currentPrice || coinPrices[symbol];
    const { leverage } = jobConfig[accountName][symbol];
    const oldPrice = openTrades[accountName][symbol].price;
    const mod = 1;
    let profit = ((newSide == 'long' ? (oldPrice / currentPrice - mod) : (currentPrice / oldPrice - mod))*100);
    if (!isNaN(profit)) {
        profit = parseFloat((profit*leverage).toFixed(2));
    } else {
        profit = null;
    }

    return profit;
  }

  const calculateStats = (accountName, symbol, history) => {
    return {
        total: (_.sum(history)).toFixed(2) + '%',
        avg: (_.mean(history)).toFixed(2) + '%',
        max: (_.max(history)).toFixed(2) + '%',
        min: (_.min(history)).toFixed(2) + '%',
        count: history.length,
        leverage: jobConfig[accountName][symbol].leverage
    }
  }

  const openPosition = async (accountName, symbol, newSide, currentPrice) => {
    let profit = getPositionProfit(accountName, symbol);
    const minimumProfit = 2.5;

    if (!currentPrice) {
        return;
    }
    // Check if symbol has a position open already AND currently less than x% profit and not in a loss. If so, ignore
    // TODO: Not sure if this is the right solution, may be able to use other indicators instead
    if (profit !== null && profit > -0.9 && profit < minimumProfit) {
        console.log('Skipping position due to low profit of ' + profit + ': ' + symbol + ' ' + newSide);
        return;
    }

    if (profit !== null) {
        profitHistory.push(profit);
    }

    const { leverage, papertrade } = jobConfig[accountName][symbol];
    var stats = {};

    // Calculate current stats IF we have more than 1 trade
    if (profitHistory.length > 0) {
        stats = calculateStats(accountName, symbol, profitHistory);
    }

    if (!papertrade) {
        await executeTrade(newSide, accountName, symbol);
    }

    const message = (papertrade ? 'PAPER ' : '') + accountName + ' ' + newSide + ' ' + symbol + ' @ ' + currentPrice + ' on ' + moment().format('h:mma MMM Do') + (profit ? '. Last profit: ' + (profit) + '% at ' + leverage + 'x' : '')
    + (stats.total ? ('\r\nTotal profit: ' + stats.total + ', Avg profit: ' + stats.avg + ', Max profit: ' + stats.max + ', Max loss: ' + stats.min + ', num trades: ' + stats.count) : '');
    console.log(message, stats);
    
    openTrades[accountName][symbol].side = newSide;
    openTrades[accountName][symbol].price = currentPrice;
    previousSignal[symbol] = newSide;

    // Send alert to discord (if not first run)
    if (!jobConfig[accountName][symbol].backtest) {
        // console.log('Sending webhook....');
        await axios.post('http://localhost:3003/api/post-trade/a879a9s8df7a98', {accountName, symbol, stats, newSide, profit, papertrade})
    }
  }

  const executeTrade = async (side, accountName, symbol) => {
    const { leverage, orderType } = jobConfig[accountName][symbol];
    // Check config to see if we are market buying/selling. If false, use limit price and adjust it based on direction
    let price = orderType === 'market' ? 0 : coinPrices[symbol];
    const qty = getCalculatedQty(accountName, symbol);

    if (side == 'short') {
        // Add 0.5% of current price so we limit sell
        price = price + (price / 500);
        const resp = await serviceClasses[accountName].sell(symbol, price, qty);
        console.log('Opening short ' + accountName + ' ' + symbol + ' ' + leverage + 'x @ ' + price, resp);
    }

    if (side == 'long') {
        // Subtract 0.5% of current price so we limit buy
        price = price - (price / 500);
        const resp = await serviceClasses[accountName].buy(symbol, price, qty);
        console.log('Opening long ' + accountName + ' ' + symbol + ' ' + leverage + 'x @ ' + price, resp);
    }
  };

  // Using the job config & User's current margin, calculate the quantity for given symbol
  const getCalculatedQty = (accountName, symbol) => {
      let { margin, leverage, contractQty } = jobConfig[accountName][symbol];

      if (!contractQty) {
        // If User specified a % margin to use, calculate using current margin
        if (isNaN(margin) && margin.includes('%')) {
            // TODO: Get user's current margin (we should have a job that runs every minute and checks margin, saving to config to speed up)
            // 1 contract = $1, so 3800 contracts = 1btc at 1x at $3800
            // (6795-6705)*10/6705=1.34% x 10 = 13.4%

            margin = 0.1;
        }

        // Calculate # of contracts using:
        contractQty = (coinPrices[symbol] / 10) * leverage;
      }

      return contractQty;
  }

  const setupIndicators = (symbol) => {
    currentIndicators[symbol] = {
        ema: 0,
        sma: 0,
        supertrend: {
            trendUp: 0,
            trendDown: 0,
            trend: 0
        },
        lastSupertrend: {
            trendUp: 0,
            trendDown: 0
        },
        lastCandleClose: 0
    };
  }

  const isClosing = (timestamp, seconds) => {
      // Compare timestamp to current time and check if seconds is less than or equal to threshold
      return moment(timestamp).diff(moment(), 'seconds') <= seconds;
  }

module.exports = {
    /* TODO: How to make this dynamic & reusable?
    - Have a "before()" function which return getIndicators data and also calls setupTrades
    - Modify getIndicators to loop through config and only get the indicators in there. Should allow for multiple same indicators, so e.g.:
        {ema1: {indicator: 'EMA', period: 5},ema2: {indicator: 'EMA', period; 8}}
    - Each strat function will perform the actual check using indicator data "if data.ema1 above data.ema2, go long"
    - Generic "open position" function
    */
    scalpStrat: async (symbol, timeframe, config) => {
        if (!currentIndicators[symbol]) {
            setupIndicators(symbol);
        }
        setupTrades(symbol, config);

        const fromTime = 1564700400; // 1st August
        const toTime = 1566319500; // 20th August
        const start = new Date().getTime();
        const chart = parseChartData(require('./3mchart.json')); // await getBitmexChart(symbol, '3m', fromTime, toTime);
        const chart10m = chart; // parseChartData(require('./10mchart.json')); // await getBitmexChart(symbol, '10m', fromTime, toTime);;
        // FUNCTIONS
        const ema = (values, period) => EMA.calculate({ values, period, reversedInput: true }).pop();
        const sma = (values, period) => SMA.calculate({ values, period, reversedInput: true }).pop();
        const cross = (prevVal, newVal, crossVal) => (prevVal <= crossVal && newVal > crossVal) || (prevVal >= crossVal && newVal < crossVal);

        // INPUTS
        const length = 50;
        const mult = 1;
        const gHistory = [];
        const gHistory10m = [];
        const c = [];
        const d = [];
        const c10m = [];
        const d10m = [];
        const src = [];
        let tt_c = [];
        const vline = [];
        const signal = [];
        const mosc = [];
        const profitHistory = [];
        // BACKTEST

        // TODO: Loop through the 10m chart and calculate the trend. Build an object of indicator values
        // where the timestmap is the key "12412321": { g, e, f }
        // Then, in the next loop below, loop through the object keys of 10m (parse int). If current time is greater than previous, but less than current - this is the nearest
        // Can then get the indicator values using data[timestamp]
        let i = 0;
        const indicator = {};
        const start10 = new Date().getTime();
        while (++i < chart10m.close.length) {
            const close = _.take(chart10m.close, i + 1);
            const open = _.take(chart10m.open, i + 1);
            const high = _.take(chart10m.high, i + 1);
            const low = _.take(chart10m.low, i + 1);

            const minOpen = _.min(open);
            const minClose = _.min(close);
            const maxOpen = _.max(open);
            const maxClose = _.max(close);
            const start = (i - length) < 0 ? 0 : (i - length);
            const highestHigh = _.max(high.slice(start, i));
            const lowestLow = _.min(low.slice(start, i));
            const a = highestHigh - Math.max(maxClose, maxOpen);
            const b = Math.min(minClose, minOpen) - lowestLow;
            c10m.push(Math.max(maxClose, maxOpen) + (a * mult));
            d10m.push(Math.min(minClose, minOpen) - (b * mult));

            const e = sma(c10m, length);
            const f = sma(d10m, length);
            const previousG = _.last(gHistory10m);
            const g = cross(close[i],close[i - 1],e) ? 1 : cross(close[i], close[i - 1],f) ? 0 : previousG;
            gHistory10m.push(g);
            indicator[chart10m.timestamp[i]] = { g, e, f, time: new Date(chart10m.timestamp[i] * 1000).toISOString() };
        }
        console.log('FINISHED generating 10m data. Duration: ' + (new Date().getTime() - start10));
        const indicators = _.map(Object.keys(indicator), i => parseInt(i));

        i = 0;
        while (++i < chart.close.length) {
            // TODO: Find the nearest record in 10m using timestamp. We'll need the index, so we can grab the previous record
            // This will be used to determine the trend, so it needs to align with 3m TF
            const close = _.take(chart.close, i + 1);
            const open = _.take(chart.open, i + 1);
            const high = _.take(chart.high, i + 1);
            const low = _.take(chart.low, i + 1);
            const time = chart.timestamp[i];
            // Find nearest timestamp
            let indicatorCount = 0;
            let indicatorKey = null;
            while (++indicatorCount < indicators.length) {
                if (time > indicators[indicatorCount - 1] && time <= indicators[indicatorCount] && indicator[indicators[indicatorCount]]) {
                    indicatorKey = indicators[indicatorCount];
                    break;
                }
            }
            // If no indicator key, use the last
            if (!indicatorKey) {
                indicatorKey = _.last(indicators);
            }
            const currentIndicator = indicator[indicatorKey];
            // ------------ TREND ----------------------
            // This requires access to a few arrays, and it also needs to push and reference data from other arrays. Hard to split into own function
            // Unless we made those arrays part of state/class
            const minOpen = _.min(open);
            const minClose = _.min(close);
            const maxOpen = _.max(open);
            const maxClose = _.max(close);
            const start = (i - length) < 0 ? 0 : (i - length);
            const highestHigh = _.max(high.slice(start, i));
            const lowestLow = _.min(low.slice(start, i));
            const a = highestHigh - Math.max(maxClose, maxOpen);
            const b = Math.min(minClose, minOpen) - lowestLow;
            c.push(Math.max(maxClose, maxOpen) + (a * mult));
            d.push(Math.min(minClose, minOpen) - (b * mult));

            const e = sma(c, length);
            const f = sma(d, length);
            const previousG = _.last(gHistory);
            const g = cross(close[i],close[i - 1],e) ? 1 : cross(close[i], close[i - 1],f) ? 0 : previousG;
            gHistory.push(g);

            // -------------- OSCILLATOR ----------------
            src.push((high[i] + low[i] + close[i]) / 3)
            const lenmon = 14
            let val = 0;
            for (let j = 0; j <= lenmon ; j++) {
                const s0 = _.last(src)
                const si = src[i - j] || 0
                const t_c = s0 > si ? 1 : s0 < si ? -1 : 0
                val += t_c
                // console.log({ s0, si, t_c, val, index: (i - j), j });
            }
            tt_c.push(val);
            // console.log('TTC AFTER: ', tt_c.slice(tt_c.length - 20, tt_c.length - 1).join(','));

            const v = ema(tt_c, 5);
            if (v !== null && typeof v !== 'undefined') {
                vline.push(v);
            }
            const m = ema(vline, 3);
            if (m !== null && typeof m !== 'undefined') {
                mosc.push(m);
            }
            const s = ema(mosc, 3);
            if (s !== null && typeof s !== 'undefined') {
                signal.push(s);
            }
            const mLast = mosc.length - 1;
            const latestSignal = _.last(signal);
            const currentMosc = mosc[mLast];

            // ------------ TRIGGER LOGIC --------------------
            // Use trend from 10m chart
            const g10m = currentIndicator.g;
            // g=0, sig=10.8, mosc now 10.3, was 11.6
            // SHORT = signal greater than 10 AND mosc was greater than signal, but is now less
            // if 10.8 >= 10 && 10.3 < 10.8 && 11.6 > 10.8
            // if 11.3 >= 10 && 10.3 < 11.3 && 11.6 > 11.3
            const short = g10m !== 1 && latestSignal >= 10 && currentMosc < latestSignal && mosc[mLast - 1] >= latestSignal
            const long = g10m === 1 && latestSignal <= -10 && currentMosc > latestSignal && mosc[mLast - 1] <= latestSignal;

            const previousProfit = (getPositionProfit(config.account, symbol, close[i]) || 0.0);
            // Check TP logic
            if (openTrades[config.account][symbol].side === 'short' && previousProfit >= 3) {
                profitHistory.push(previousProfit);
                openTrades[config.account][symbol].side = null;
                openTrades[config.account][symbol].price = null;
                console.log(new Date(chart.timestamp[i] * 1000).toISOString() + ' *** TP HIT, CLOSING SHORT! Profit: ' + previousProfit + ', closed at: ' + close[i]);
            } else if (openTrades[config.account][symbol].side === 'long' && previousProfit >= 3) {
                profitHistory.push(previousProfit);
                openTrades[config.account][symbol].side = null;
                openTrades[config.account][symbol].price = null;
                console.log(new Date(chart.timestamp[i] * 1000).toISOString() + ' *** TP HIT, CLOSING LONG! Profit: ' + previousProfit + ', closed at: ' + close[i]);
            }

            // SL logic
            if (previousProfit <= -0.8) {
                profitHistory.push(previousProfit);
                const side = openTrades[config.account][symbol].side.toUpperCase();
                openTrades[config.account][symbol].side = null;
                openTrades[config.account][symbol].price = null;
                console.log(new Date(chart.timestamp[i] * 1000).toISOString() + ' *** STOP HIT, CLOSING ' + side + '! LOSS: ' + previousProfit + ', closed at: ' + close[i]);
            }

            if (long || short) {
                // console.log(new Date(chart.timestamp[i] * 1000).toISOString() + (g10m === 1 ? ' LONG TREND ' : ' SHORT '), JSON.stringify({ g, c2: close[i - 1], c: close[i], e, sig: (latestSignal || -1).toFixed(1), msc: (currentMosc || -1).toFixed(1), f, previousMosc: mosc[mLast - 1] }));
                // If flipping position, calculate profit
                if (openTrades[config.account][symbol].side === 'short' && long) {
                    profitHistory.push(previousProfit);
                    openTrades[config.account][symbol].side = 'long';
                    openTrades[config.account][symbol].price = close[i];
                    console.log(new Date(chart.timestamp[i] * 1000).toISOString() + ' FLIPPING SHORT TO LONG. Profit: ' + previousProfit, openTrades[config.account][symbol]);
                } else if (openTrades[config.account][symbol].side === 'long' && short) {
                    profitHistory.push(previousProfit);
                    openTrades[config.account][symbol].side = 'short';
                    openTrades[config.account][symbol].price = close[i];
                    console.log(new Date(chart.timestamp[i] * 1000).toISOString() + ' FIPPING LONG TO SHORT. Profit: ' + previousProfit, openTrades[config.account][symbol]);
                } else if (!openTrades[config.account][symbol].side) {
                    openTrades[config.account][symbol].side = (long ? 'long' : 'short');
                    openTrades[config.account][symbol].price = close[i];
                    console.log(new Date(chart.timestamp[i] * 1000).toISOString() + ' OPENING NEW POSITION.', openTrades[config.account][symbol]);
                }
            }
        }
        // Close current position
        if (openTrades[config.account][symbol].side) {
            const previousProfit = (getPositionProfit(config.account, symbol, _.last(chart.close)) || 0.0);
            profitHistory.push(previousProfit);
            console.log('Closing final position ' + openTrades[config.account][symbol].side + ' at ' + _.last(chart.close) + ', profit: ' + previousProfit);
            openTrades[config.account][symbol] = { side: null, price: null };
        }
        const stats = calculateStats(config.account, symbol, profitHistory);
        console.log('*** Strat complete. Ran from ' + new Date(fromTime * 1000).toISOString() + ' to ' + new Date(toTime * 1000).toISOString() + ', candles: ' + chart.open.length + ', duration: ' + (new Date().getTime() - start), stats);
        process.exit(1);
    },
    emasmaStrat: async (symbol, timeframe, config, job) => {
        const account = accountConfig[config.account];
        // If no account found, quit
        if (!account) {
            console.log('emasmaStrat account not found: ' + config.account + '. Quitting.....');
            return;
        }

        const { exchange } = account;
        // console.log('EMA: ' + ema + ', SMA: ' + sma + ', SYM: ' + symbol + ', PRICE: ' + coinPrices[symbol] + ' - ' + moment().format('dddd MMMM Do YYYY @ h:mma'))
        // If no current indicators for this coin, set and skip. We don't want to enter a trade
        // Unless we know what the previous value was
        if (!currentIndicators[symbol]) {
            setupIndicators(symbol);
            console.log(config.account + ' Setup EMA/SMA strat for "' + symbol + '" timeframe "' + timeframe + '" on exchange "' + exchange + '"');
        }

        setupTrades(symbol, config);
        const { ema, sma, chart } = await getIndicators(symbol, timeframe, config);

        let newSide = null;
        currentIndicators[symbol].ema = ema;
        currentIndicators[symbol].sma = sma;
        const backTest = jobConfig[config.account][symbol].backtest;

        if (backTest) {
            let backtestProfit = [];
            let emaHistory = EMA.calculate({values: chart.close, period: config.ema});
            let smaHistory = SMA.calculate({values: chart.close, period: config.sma});
            // EMA or SMA length may be less than chart, find the smallest length and split arrays to match it
            let max = emaHistory.length < smaHistory.length ? emaHistory.length : smaHistory.length;
            let offset = chart.close.length - max;
            chart.close = _.slice(chart.close, offset);
            chart.timestamp = _.slice(chart.timestamp, offset);
            emaHistory = _.slice(emaHistory, emaHistory.length-max);
            smaHistory = _.slice(smaHistory, smaHistory.length-max);
            let i = 0;
            let previousEma = 0;
            let previousSma = 0;
            console.log('\r\n******** BEGIN BACKTEST ' + symbol + ' ' + config.account + ' ' + timeframe + ' ********* \r\n')
            for (i; i < chart.close.length; i++) {
                let price = chart.close[i].toFixed(2);
                if (!emaHistory[i] || !smaHistory[i]) {
                    break;
                }
                // Check for LONG signal - if EMA was below SMA but has now crossed over AND not currently long
                if (previousEma < previousSma &&  emaHistory[i] > smaHistory[i] && openTrades[config.account][symbol].side != 'long') {
                    const previousProfit = (getPositionProfit(config.account, symbol, price) || 0.0);
                    backtestProfit.push(previousProfit);
                    openTrades[config.account][symbol].side = 'long';
                    openTrades[config.account][symbol].price = price;
                    console.log(i + ' EMA above SMA, long, EMA: ' + emaHistory[i].toFixed(2) + ', SMA: ' + smaHistory[i].toFixed(2) + ', CLOSE: ' + price + ' - ' + moment.utc(chart.timestamp[i]).format('Do MMM h:mma'));
                }

                // Check for SHORT signal - if EMA was above SMA but has now crossed under AND not currently short
                else if (previousEma > previousSma &&  emaHistory[i] < smaHistory[i] && openTrades[config.account][symbol].side != 'short') {
                    const previousProfit = (getPositionProfit(config.account, symbol, price) || 0.0);
                    backtestProfit.push(previousProfit);
                    openTrades[config.account][symbol].side = 'short';
                    openTrades[config.account][symbol].price = price;
                    console.log(i + ' EMA below SMA, short, EMA: ' + emaHistory[i].toFixed(2) + ', SMA: ' + smaHistory[i].toFixed(2) + ', CLOSE: ' + price + ' - ' + moment.utc(chart.timestamp[i]).format('Do MMM h:mma'));
                }
                previousEma = emaHistory[i];
                previousSma = smaHistory[i];
            }
            // Turn off backtest after it's run
            const stats = calculateStats(config.account, symbol, backtestProfit);
            console.log('\r\n')
            console.log(JSON.stringify(stats));
            jobConfig[config.account][symbol].backtest = false;
            console.log('\r\n******** BACKTEST COMPLETE --  ' + symbol + ' ' + config.account + ' ' + timeframe + ' *********')
        }

        // Check for LONG signal - if EMA was below SMA but has now crossed over AND not currently long
        if (ema >= sma && openTrades[config.account][symbol].side != 'long') {
            newSide = 'long';
        }

        // Check for SHORT signal - if EMA was above SMA but has now crossed under AND not currently short
        if (ema <= sma && openTrades[config.account][symbol].side != 'short') {
            newSide = 'short';
        }

        if (newSide) {
            console.log('EMA STRAT ' + symbol + ' ' + newSide + ' ' + coinPrices[symbol]);
            openPosition(config.account, symbol, newSide, coinPrices[symbol]);
        }
    },

    guppyStrat: async (symbol, timeframe, config, job) => {
        const account = accountConfig[config.account];

        // If no account found, quit
        if (!account) {
            console.log('superTrend account not found: ' + config.account + '. Quitting.....');
            return;
        }

        const { exchange } = account;
        setupTrades(symbol, config);
        let data = await getIndicators(symbol, timeframe, config);
        let newSide = null;

        // If no current indicators for this coin, set and skip. We don't want to enter a trade
        // Unless we know what the previous value was
        if (!currentIndicators[symbol]) {
            setupIndicators(symbol);
            console.log(config.account + ' Setup Guppy strat for "' + symbol + '" timeframe "' + timeframe + '" on exchange "' + exchange + '"');
        }

        let ema200 = format(EMA.calculate({values: data.chart.close, period: 200}).pop());

        let i = data.chart.open.length-1;
        let candle = {
            open: data.chart.open[i],
            high: data.chart.high[i],
            low: data.chart.low[i],
            close: data.chart.close[i],
            timestamp: data.chart.timestamp[i]
        };

        let fastEmas = {};
        let slowEmas = {};
        
        for (j = 1; j <= 22; j++) {
            if (j < 8) {
                fastEmas[j] = format(EMA.calculate({values: data.chart.close, period: j*3}).pop());
            } else {
                slowEmas[j] = format(EMA.calculate({values: data.chart.close, period: j*3}).pop());
            }
        }

        //Fast EMA Color Rules
        let fastEmaLong = (fastEmas[1] > fastEmas[2] && fastEmas[2] > fastEmas[3] && fastEmas[3] > fastEmas[4] && fastEmas[4] > fastEmas[5] && fastEmas[5] > fastEmas[6] && fastEmas[6] > fastEmas[7])
        let fastEmaShort = (fastEmas[1] < fastEmas[2] && fastEmas[2] < fastEmas[3] && fastEmas[3] < fastEmas[4] && fastEmas[4] < fastEmas[5] && fastEmas[5] < fastEmas[6] && fastEmas[6] < fastEmas[7])
        
        //Slow EMA Color Rules
        let slowEmaLong = slowEmas[8] > slowEmas[9] && slowEmas[9] > slowEmas[10] && slowEmas[10] > slowEmas[11] && slowEmas[11] > slowEmas[12] && slowEmas[12] > slowEmas[13] && slowEmas[13] > slowEmas[14] && slowEmas[14] > slowEmas[15] && slowEmas[15] > slowEmas[16] && slowEmas[16] > slowEmas[17] && slowEmas[17] > slowEmas[18] && slowEmas[18] > slowEmas[19] && slowEmas[19] > slowEmas[20] && slowEmas[20] > slowEmas[21] && slowEmas[21] > slowEmas[22]
        let slowEmaShort = slowEmas[8] < slowEmas[9] && slowEmas[9] < slowEmas[10] && slowEmas[10] < slowEmas[11] && slowEmas[11] < slowEmas[12] && slowEmas[12] < slowEmas[13] && slowEmas[13] < slowEmas[14] && slowEmas[14] < slowEmas[15] && slowEmas[15] < slowEmas[16] && slowEmas[16] < slowEmas[17] && slowEmas[17] < slowEmas[18] && slowEmas[18] < slowEmas[19] && slowEmas[19] < slowEmas[20] && slowEmas[20] < slowEmas[21] && slowEmas[21] < slowEmas[22] 
        
        //Fast EMA Final Color Rules
        newSide = (fastEmaLong && slowEmaLong && coinPrices[symbol] > ema200) ? 'long' : (fastEmaShort && slowEmaShort && coinPrices[symbol] < ema200) ? 'short' : null
        
        // Only check slow EMA for colors otherwise the fast EMA throws false signals
        currentIndicators[symbol].guppy =  slowEmaLong ? 'green' :  slowEmaShort? 'red' : 'gray';

        if (newSide && openTrades[config.account][symbol].side !== newSide) {
            console.log(symbol + ' ' + newSide + ' - price above 200ema? ' + (coinPrices[symbol] > ema200) + '. EMA200: ' + ema200 + ', Current: ' + coinPrices[symbol]);
            console.log('FAST EMA: ' + JSON.stringify(fastEmas));
            console.log('SLOW EMAS: ' + JSON.stringify(slowEmas));
            openPosition(config.account, symbol, newSide, coinPrices[symbol]);
        }

        // If guppy has turned grey and we have a position open, close it
        if (openTrades[config.account][symbol].side && currentIndicators[symbol].guppy == 'gray') {
            // console.log(symbol + ' guppy has turned gray (current side: ' + openTrades[config.account][symbol].side + '). Price: ' + coinPrices[symbol] +);
            // closePosition(config.account, symbol, coinPrices[symbol], 'close');
            console.log(symbol + ' Guppy is neutral. Price above 200ema? ' + (coinPrices[symbol] > ema200) + '. EMA200: ' + ema200 + ', Current: ' + coinPrices[symbol] + ' - ' + moment().format('Do MMM hh:mma'));
            // console.log('FAST EMA: ' + JSON.stringify(fastEmas));
            // console.log('SLOW EMAS: ' + JSON.stringify(slowEmas));
        }

        checkProfitLoss(config.account, symbol, coinPrices[symbol]);
    }
}