// To be commented.
var _ = require('lodash');
const fs  = require('fs');

var allStrategies = new Map();

fs.readdir("./src/server/lib/strategies", (err, files) => {
  files.forEach(file => {
	
  	if (file == 'main.js'){return;}

	var commandName = file.split(".")[0]
	var importedCommandsParams = require("./" + file )
	console.log('FILE: ', file, commandName, importedCommandsParams);
    allStrategies.set(commandName, importedCommandsParams.init )
  });
})

function DeleteDesignedMessage(test){
  test.delete().catch(console.error);
}

var testFolder = null;

module.exports = {
	execute : async function(message, prefix, client){
		if(message.author.bot) return;

	 	if(message.content.indexOf(prefix) !== 0) return;

	 	const args = message.content.slice(prefix.length).trim().split(/ +/g);
	 	const command = args.shift().toLowerCase();

	 	allStrategies.forEach(async function(v, k, m){
	 		if (k == command){

				// If commands can be channel restricted, make sure this is the right channel
	 			if (v.channels != null && v.channels.indexOf(message.channel.name) === -1){
	 				return;
	 			}

				// Make sure user has permission to this command
	 			if (v.groups != null && (!message.member.roles.some(role => v.groups.includes(role.name)))){
					return;
	 			}

				 // Check if devmode is on. If so, ignore command
	 			if (v.dev != null && devMode !== 1){
	 				if (devMode == 1){
						// nothing for now
	 				}else{
	 					message.reply("Dev mode is active. (dev = 1)")
	 					return;
	 				}
	 			}

	 			v.execute(message, args, client);
	 		
	 			if (v.delete){
	 				DeleteDesignedMessage(message);
	 			}
	 		}
	 	});
	}
}
