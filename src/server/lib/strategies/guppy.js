const Discord = require("discord.js");

guppyStrat: async (symbol, timeframe, config, job) => {
    const account = accountConfig[config.account];

    // If no account found, quit
    if (!account) {
        console.log('superTrend account not found: ' + config.account + '. Quitting.....');
        return;
    }

    const { exchange } = account;
    setupTrades(symbol, config);
    let data = await getIndicators(symbol, timeframe, config);
    var newSide = null;

    // If no current indicators for this coin, set and skip. We don't want to enter a trade
    // Unless we know what the previous value was
    if (!currentIndicators[symbol]) {
        setupIndicators(symbol);
        console.log(config.account + ' Setup Guppy strat for "' + symbol + '" timeframe "' + timeframe + '" on exchange "' + exchange + '"');
    }

    let ema200 = format(EMA.calculate({values: data.chart.close, period: 200}).pop());

    let i = data.chart.open.length-1;
    let candle = {
        open: data.chart.open[i],
        high: data.chart.high[i],
        low: data.chart.low[i],
        close: data.chart.close[i],
        timestamp: data.chart.timestamp[i]
    };

    let fastEmas = {};
    let slowEmas = {};
    
    for (j = 1; j <= 22; j++) {
        if (j < 8) {
            fastEmas[j] = format(EMA.calculate({values: data.chart.close, period: j*3}).pop());
        } else {
            slowEmas[j] = format(EMA.calculate({values: data.chart.close, period: j*3}).pop());
        }
    }

    //Fast EMA Color Rules
    let fastEmaLong = (fastEmas[1] > fastEmas[2] && fastEmas[2] > fastEmas[3] && fastEmas[3] > fastEmas[4] && fastEmas[4] > fastEmas[5] && fastEmas[5] > fastEmas[6] && fastEmas[6] > fastEmas[7])
    let fastEmaShort = (fastEmas[1] < fastEmas[2] && fastEmas[2] < fastEmas[3] && fastEmas[3] < fastEmas[4] && fastEmas[4] < fastEmas[5] && fastEmas[5] < fastEmas[6] && fastEmas[6] < fastEmas[7])
    
    //Slow EMA Color Rules
    let slowEmaLong = slowEmas[8] > slowEmas[9] && slowEmas[9] > slowEmas[10] && slowEmas[10] > slowEmas[11] && slowEmas[11] > slowEmas[12] && slowEmas[12] > slowEmas[13] && slowEmas[13] > slowEmas[14] && slowEmas[14] > slowEmas[15] && slowEmas[15] > slowEmas[16] && slowEmas[16] > slowEmas[17] && slowEmas[17] > slowEmas[18] && slowEmas[18] > slowEmas[19] && slowEmas[19] > slowEmas[20] && slowEmas[20] > slowEmas[21] && slowEmas[21] > slowEmas[22]
    let slowEmaShort = slowEmas[8] < slowEmas[9] && slowEmas[9] < slowEmas[10] && slowEmas[10] < slowEmas[11] && slowEmas[11] < slowEmas[12] && slowEmas[12] < slowEmas[13] && slowEmas[13] < slowEmas[14] && slowEmas[14] < slowEmas[15] && slowEmas[15] < slowEmas[16] && slowEmas[16] < slowEmas[17] && slowEmas[17] < slowEmas[18] && slowEmas[18] < slowEmas[19] && slowEmas[19] < slowEmas[20] && slowEmas[20] < slowEmas[21] && slowEmas[21] < slowEmas[22] 
    
    //Fast EMA Final Color Rules
    let newSide = fastEmaLong && slowEmaLong ? 'long' : fastEmaShort && slowEmaShort? 'short' : null
    
    currentIndicators[symbol].guppy = fastEmaLong && slowEmaLong ? 'green' : fastEmaShort && slowEmaShort? 'red' : 'gray';

    if (newSide && openTrades[config.account][symbol].side !== newSide) {
        console.log(symbol + ' ' + newSide + ' - price above 200ema? ' + (coinPrices[symbol] > ema200));
        openPosition(config.account, symbol, newSide, coinPrices[symbol]);
    }

    if (openTrades[config.account][symbol].side && !newSide && currentIndicators[symbol].guppy != 'gray') {
        console.log(symbol + ' Guppy is now GRAY. Price above 200ema? ' + (coinPrices[symbol] > ema200) + ' - ' + moment().format('Do MMM hh:mma'));
    }

    checkProfitLoss(config.account, symbol, coinPrices[symbol]);
}

module.exports = {
    init: {
        execute: async (message, args, client) => {
            console.log('adduser command: ', args)
        },
        groups: ['Developer', 'Moderator']
    }
}
