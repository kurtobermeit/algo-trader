//@version=3
strategy("just stuff", shorttitle="stuff", precision = 5, overlay=true)
length = input(50),mult = input(1,minval=0,maxval=1),Method = input("SMA",options=["SMA","Median"])
//----
Avg(x)=> Method == "SMA" ? sma(x,length) : percentile_linear_interpolation(x,length,50)
//----
a = highest(length) - max(close,open)
b = min(close,open) - lowest(length)
c = max(close,open) + a*mult
d = min(close,open) - b*mult
//----
e = Avg(c)
f = Avg(d)
g = 0
g := cross(close,e) ? 1 : cross(close,f) ? 0 : nz(g[1])
//---
hilo = g*f+(1-g)*e
css = g == 1 ? #0080FF : #FE2E64

// -----------------------------------------------------


lenmon = 14
src = hlc3

ti() =>
    tt_c = 0
    for i = 1 to lenmon
        s0 = src[0]
        si = src[i]
        t_c = s0 > si ? 1 :s0 < si ? -1 : 0
        tt_c := tt_c + t_c
        tt_c
    tt_c


vline=ema(ti(), 5)
mosc=ema(vline, 3)
signal=ema(mosc, 3)

cblue=#008FFF
corange=#E0670E
cgreen = #5F930A
cred = #F93A00


div_max = lenmon * 0.715

//plot(mosc, color=mosc > signal? #1e9b25 : #FF0000 , linewidth = 3, title="Sigline", transp=0)

//plotshape(close, color = mosc > signal ? lime : orange, location = location.abovebar, style = shape.triangledown, size = size.normal)
//plotshape(div_max * -1, color = lime, location = location.belowbar, style = shape.triangleup, size = size.normal)
//pmax = plot(max(div_max, mosc), color = na, title="",editable=false)
//pmin = plot(min(div_max * -1, mosc), color = na, title="",editable=false)

//plotshape(close, color = mosc > 10 ? red : green, location = location.abovebar, style = shape.triangledown, size = size.normal)

plotshape(g != 1 and signal > 10 and crossunder(mosc,signal), color = orange, location = location.abovebar, style = shape.triangledown, size = size.normal)
plotshape(g == 1 and signal < -10 and crossover(mosc,signal), color = lime, location = location.belowbar, style = shape.triangleup, size = size.normal)


strategy.entry("Short", strategy.short, when=g != 1 and signal > 10 and mosc < signal)
strategy.entry("Long", strategy.long, when=g == 1 and signal < -10 and mosc > signal)


//strategy.exit("Exit Long", from_entry = "Long", profit= (close < 1000 ? 5500 : 5500))
//strategy.close("Long", when= g != 1)
//strategy.close("Short", when= g == 1)